<?php
/**
 * Created by PhpStorm.
 * User: HAPXu3
 * Date: 01.10.2018
 * Time: 23:18
 */
declare(strict_types=1);

namespace app\models\links;

use yii\base\InvalidArgumentException;

class TagsCollection
{
    /** @var array $tags */
    private $tags = [];

    /**
     * TagsCollection constructor. Accepts an array of type Tag or string.
     * @param array $tags
     */
    public function __construct(array $tags)
    {
        foreach ($tags as $tag) {
            if ($tag instanceof Tag) {
                $this->tags[] = $tag;
            } elseif (is_string($tag)) {
                $this->tags[] = new Tag($tag);
            } else {
                throw new InvalidArgumentException('Tag must be of type Tag or string.');
            }
        }
    }

    /**
     * @param Tag $tag
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;
    }

    /**
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        foreach ($this->tags as $index => $tagInCollect) {
            /** @var Tag $tagInCollect */
            if ($tag->equal($tagInCollect)) {
                unset($this->tags[$index]);
            }
        }
    }
}