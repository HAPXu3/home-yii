<?php
/**
 * Created by PhpStorm.
 * User: HAPXu3
 * Date: 25.07.2018
 * Time: 21:53
 */

namespace app\assets;

use yii\web\AssetBundle,
    yii\web\YiiAsset,
    yii\bootstrap\BootstrapAsset;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/site.css',
    ];

    public $js = [];

    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
    ];
}