<?php
return [
    'id' => 'home-app',
    'name' => 'O_o',
    'basePath' => realpath(__DIR__ . '/../'),
    'timeZone' => 'Asia/Vladivostok',
    'components' =>[
        'request' => [
            'cookieValidationKey' => '',
            'enableCookieValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                ['class' => 'yii\rest\UrlRule', 'controller' => 'api/link'],
            ],
        ],
        'response' => [
            'formatters' => [
                'yaml' => [
                    'class' => \app\utilities\YamlResponseFormatter::class,
                ],
            ],
        ],
        'user' => [
            'identityClass' => \app\models\user\User::class,
        ],
    ],
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['*'],
        ],
    ],
    'extensions' => require(__DIR__ . '/../vendor/yiisoft/extensions.php'),
];