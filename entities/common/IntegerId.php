<?php
/**
 * Created by PhpStorm.
 * User: HAPXu3
 * Date: 02.10.2018
 * Time: 15:37
 */

namespace app\entities\common;


class IntegerId implements EntityIdInterface
{
    private $id;

    public function __construct(int $number)
    {
        if ($number <= 0) {
            throw new \OutOfRangeException('Id must be a positive integer.');
        }
        $this->id = $number;
    }

    public function getId(): int
    {
        return $this->id;
    }
}