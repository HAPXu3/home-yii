<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180707_070937_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->unique()->notNull(),
            'password' => $this->string()->notNull(),
            'auth_key' => $this->string()->unique(),
            'access_token' => $this->string()->unique(),
            'date_create' => $this->dateTime()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
