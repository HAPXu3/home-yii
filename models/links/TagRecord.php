<?php
/**
 * Created by PhpStorm.
 * User: HAPXu3
 * Date: 26.10.2018
 * Time: 9:57
 */

namespace app\models\links;

use yii\db\ActiveRecord;

/**
 * Class TagRecord
 * @package app\models\links
 * @property int $id
 * @property string $tag
 */
class TagRecord extends ActiveRecord
{
    public static function tableName()
    {
        return 'link_tags';
    }

    public function rules()
    {
        return [
            ['tag', 'required'],
            ['tag', 'filter', 'filter' => 'trim'],
            ['tag', 'filter', 'filter' => 'strtolower'],
            ['tag', 'string', 'max' => 255],
        ];
    }


}