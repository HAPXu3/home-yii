<?php

/**
 * @var \app\models\user\LoginForm $model
 * @var \yii\web\View $this
 */

use yii\helpers\Html,
    yii\bootstrap\ActiveForm;

$this->title = 'Sign in';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">

            <?php
                $form = ActiveForm::begin(['id' => 'login-form']);
                echo $form->field($model, 'login')->textInput(['autofocus' => true]);
                echo $form->field($model, 'password')->passwordInput();
                echo $form->field($model, 'rememberMe')->checkbox();
            ?>

            <div class="form-group">
                <?=Html::submitButton(
                    'Sign in',
                    ['class' => 'btn btn-primary', 'name' => 'login-button']
                )?>
            </div>

            <?php ActiveForm::end()?>
        </div>
    </div>
</div>