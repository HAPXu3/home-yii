<?php
/**
 * Created by PhpStorm.
 * User: HAPXu3
 * Date: 22.07.2018
 * Time: 14:52
 */

namespace app\models\user;

use yii\base\Model;

class SignupForm extends Model
{
    public $login;
    public $password;

    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            ['login', 'trim'],
            ['login', 'unique', 'targetClass' => User::class, 'message' => 'Похоже... этот логин занят. Похоже... фантазия у тебя не очень =|'],
            ['login', 'string', 'min' => 2, 'max' => 255],

            ['password', 'string', 'min' => 6],
        ];
    }

    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->login = $this->login;
        $user->setPassword($this->password);

        return $user->save() ? $user : null;
    }
}