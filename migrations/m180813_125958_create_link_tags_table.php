<?php

use yii\db\Migration;

/**
 * Handles the creation of table `link_tags`.
 */
class m180813_125958_create_link_tags_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('link_tags', [
            'id' => $this->primaryKey(),
            'tag' => $this->string(50),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('link_tags');
    }
}
