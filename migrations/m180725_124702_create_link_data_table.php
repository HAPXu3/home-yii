<?php

use yii\db\Migration;

/**
 * Handles the creation of table `link_data`.
 */
class m180725_124702_create_link_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('link_data', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'sort_order' => $this->integer(),
            'title' => $this->string(50),
            'full_url' => $this->text(),
            'description' => $this->text(),
            'status' => $this->tinyInteger(),
            'date_create' => $this->dateTime(),
        ]);

        //create index for column `user_id`
        $this->createIndex(
            'idx-link_data-user_id',
            'link_data',
            'user_id'
        );

        //add foreign key for table `user`
        $this->addForeignKey(
            'fk-link_data-user_id',
            'link_data',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-link_data-user_id',
            'link_data'
        );

        //drops index for column `user_id`
        $this->dropIndex(
            'idx-link_data-user_id',
            'link_data'
        );

        $this->dropTable('link_data');
    }
}
