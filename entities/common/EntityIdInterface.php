<?php
/**
 * Created by PhpStorm.
 * User: HAPXu3
 * Date: 02.10.2018
 * Time: 15:35
 */

namespace app\entities\common;


interface EntityIdInterface
{
    public function getId();
}