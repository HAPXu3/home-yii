<?php
/**
 * Created by PhpStorm.
 * User: HAPXu3
 * Date: 17.07.2018
 * Time: 22:07
 */

namespace app\models\user;

use yii\base\Model;

class LoginForm extends Model
{
    public $login;
    public $password;
    public $rememberMe;

    /** @var User */
    public $user;

    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    public function validatePassword($attributeName)
    {
        if ($this->hasErrors()) {
            return;
        }

        $user = $this->getUser($this->login);
        if (!($user and $this->isCorrectHash($this->{$attributeName}, $user->password))) {
            $this->addError('password', 'Incorrect username or password');
        }
    }

    private function getUser($login)
    {
        if (!$this->user) {
            $this->user = $this->fetchUser($login);
        }

        return $this->user;
    }

    private function fetchUser($login)
    {
        return User::findOne(compact('login'));
    }

    private function isCorrectHash($plaintext, $hash)
    {
        return \Yii::$app->security->validatePassword($plaintext, $hash);
    }

    public function login()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = $this->getUser($this->login);
        if (!$user) {
            return false;
        }

        return \Yii::$app->user->login(
            $user,
            $this->rememberMe ? 3600 * 24 * 30 : 0
        );
    }
}