<?php
/**
 * Created by PhpStorm.
 * User: HAPXu3
 * Date: 09.08.2018
 * Time: 23:05
 */
declare(strict_types=1);

namespace app\controllers\api;

use yii\rest\Controller,
    Yii,
    yii\filters\auth\HttpHeaderAuth,
    app\models\links\LinkRecord,
    app\models\links\TagRecord,
    app\models\links\Link,
    app\models\links\Tag,
    app\models\links\TagsCollection,
    app\entities\common\IntegerId,
    yii\base\InvalidValueException,
    yii\base\Model;

class LinkController extends Controller
{
    protected function verbs() : array
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
            'options' => ['OPTIONS'],
        ];
    }

    public function init()
    {
        parent::init();
        Yii::$app->user->enableSession = false;
    }

    public function actionIndex()
    {
        return 'index';
    }

    public function actionView()
    {
        return 'view';
    }

    public function actionCreate()
    {
        $userId = intval(Yii::$app->user->id);
        $linkRecord = new LinkRecord();
        $tagsFromRequest = Yii::$app->request->post('tags');
        $post = Yii::$app->request->post();

        if (!is_array($tagsFromRequest) || empty($tagsFromRequest)) {
            throw new InvalidValueException('Field "tags" cannot be empty.');
        }

        $tagRecords = [];
        foreach ($tagsFromRequest as $tagFromRequest) {
            $tagRecords[] = (new TagRecord(['tag' => $tagFromRequest]));
        }

        $tags = [];
        if (Model::validateMultiple($tagRecords)) {
            foreach ($tagRecords as $tagRecord) {
                /** @var TagRecord $tagRecord */
                // todo replace to Repository pattern
                $tags[] = (new Tag($tagRecord->getAttribute('tag')));
            }
        } else {
            // todo get errors from AR
        }

        if (empty($tags)) {
            throw new InvalidValueException('Link must be contain tags.');
        }

        $linkRecord->setAttribute('user_id', $userId);

        if ($linkRecord->load($post, '') && $linkRecord->validate()) {
            // todo replace to Repository pattern
            $link = new Link(new IntegerId($userId));
            $linkRecord->fillModel($link, new TagsCollection($tags));

            return $linkRecord->getAttributes();
        }

        $errors = $linkRecord->getErrors();
        return $errors;
    }

    public function actionUpdate()
    {
        return 'update';
    }

    public function actionDelete()
    {
        return 'delete';
    }

    public function actions() : array
    {
        return [
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    public function behaviors() : array
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator']['class'] = HttpHeaderAuth::class;

        return $behaviors;
    }
}