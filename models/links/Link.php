<?php
/**
 * Created by PhpStorm.
 * User: HAPXu3
 * Date: 25.08.2018
 * Time: 16:08
 */
declare(strict_types=1);

namespace app\models\links;

use app\entities\common\EntityIdInterface;
use yii\base\InvalidArgumentException;

class Link
{
    /** @var  EntityIdInterface $id */
    private $id;

    /** @var  EntityIdInterface $user_id */
    private $userId;

    /** @var int $sort_order */
    private $sortOrder;

    /** @var string $title */
    private $title;

    /** @var string $url */
    private $url;

    /** @var string $description */
    private $description;

    /** @var int $status */
    private $status;

    /** @var \DateTime $date_create */
    private $dateCreate;

    /** @var TagsCollection $tagsCollection */
    private $tagsCollection = [];

    /**
     * Link constructor.
     * @param EntityIdInterface|null $id
     * @param EntityIdInterface $userId
     * @param array $properties
     */
    public function __construct(EntityIdInterface $userId, EntityIdInterface $id = null, array $properties = [])
    {
        $this->setUserId($userId);
        $this->setId($id);

        if (isset($properties['sort_order'])) $this->setSortOrder($properties['sort_order']);
        if (isset($properties['title'])) $this->setTitle($properties['title']);
        if (isset($properties['full_url'])) $this->setUrl($properties['full_url']);
        if (isset($properties['description'])) $this->setDescription($properties['description']);
        if (isset($properties['status'])) $this->setStatus($properties['status']);
        if (isset($properties['tags_collection'])) $this->setTagsCollection($properties['tags_collection']);

        if (isset($properties['date_create'])) {
            if ($properties['date_create'] instanceof \DateTime) {
                $this->setDateCreate($properties['date_create']);
            } else {
                throw new InvalidArgumentException('Field "date_create" must be correct string (Y-m-d H:i:s) or DateTime');
            }
        }
    }

    public function addTag(Tag $tag)
    {
        $this->tagsCollection->addTag($tag);
    }

    /**
     * @param EntityIdInterface|null $id
     */
    public function setId(?EntityIdInterface $id)
    {
        $this->id = $id;
    }

    /**
     * @param EntityIdInterface $userId
     */
    public function setUserId(EntityIdInterface $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @param int|null $sortOrder
     */
    public function setSortOrder(?int $sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title)
    {
        $this->title = $title;
    }

    /**
     * @param string|null $url
     */
    public function setUrl(?string $url)
    {
        $this->url = $url;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description)
    {
        $this->description = $description;
    }

    /**
     * @param int|null $status
     */
    public function setStatus(?int $status)
    {
        $this->status = $status;
    }

    /**
     * @param \DateTime|null $dateCreate
     * @throws \LogicException
     */
    public function setDateCreate(?\DateTime $dateCreate)
    {
        if (!is_null($this->dateCreate)) {
            throw new \LogicException('Cannot change the date of creation');
        }
        $this->dateCreate = $dateCreate;
    }

    /**
     * @param TagsCollection|null $tagsCollection
     */
    public function setTagsCollection(?TagsCollection $tagsCollection)
    {
        $this->tagsCollection = $tagsCollection;
    }

    /**
     * @return EntityIdInterface|null
     */
    public function getId(): ?EntityIdInterface
    {
        return $this->id;
    }

    /**
     * @return EntityIdInterface
     */
    public function getUserId(): EntityIdInterface
    {
        return $this->userId;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return int|null
     */
    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateCreate(): ?\DateTime
    {
        return $this->dateCreate;
    }

    /**
     * @return TagsCollection|null
     */
    public function getTagsCollection(): ?TagsCollection
    {
        return $this->tagsCollection;
    }
}