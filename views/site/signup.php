<?php
/**
 * Created by PhpStorm.
 * User: HAPXu3
 * Date: 22.07.2018
 * Time: 16:15
 */

use yii\helpers\Html,
    yii\bootstrap\ActiveForm;

/**
 * @var \app\models\user\SignupForm $model
 * @var \yii\web\View $this
 */

$this->title = 'Sign up';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'login')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>