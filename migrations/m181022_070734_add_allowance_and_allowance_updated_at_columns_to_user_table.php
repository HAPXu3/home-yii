<?php

use yii\db\Migration;

/**
 * Handles adding allowance_and_allowance_updated_at to table `user`.
 */
class m181022_070734_add_allowance_and_allowance_updated_at_columns_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'allowance', $this->smallInteger());
        $this->addColumn('user', 'allowance_updated_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'allowance');
        $this->dropColumn('user', 'allowance_updated_at');
    }
}
