<?php
/**
 * Created by PhpStorm.
 * User: HAPXu3
 * Date: 01.10.2018
 * Time: 13:34
 */
declare(strict_types=1);

namespace app\models\links;

use app\entities\common\EntityIdInterface;

class Tag
{
    /** @var EntityIdInterface $id */
    private $id;

    /** @var string $text */
    private $text;

    public function __construct(string $text, EntityIdInterface $id = null)
    {
        $this->setId($id);
        $this->setText($text);
    }

    /**
     * @param EntityIdInterface $id
     */
    public function setId(?EntityIdInterface $id)
    {
        $this->id = $id;
    }

    /**
     * @return EntityIdInterface
     */
    public function getId(): ?EntityIdInterface
    {
        return $this->id;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param Tag $tag
     * @return bool
     */
    public function equal(Tag $tag): bool
    {
        return $this->text === $tag->text;
    }
}