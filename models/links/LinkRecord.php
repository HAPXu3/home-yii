<?php
/**
 * Created by PhpStorm.
 * User: HAPXu3
 * Date: 23.10.2018
 * Time: 17:37
 */

namespace app\models\links;

use yii\db\ActiveRecord;

/**
 * Class LinkRecord
 * @package app\models\links
 * @property integer $id
 * @property integer $user_id
 * @property integer $sort_order
 * @property string $title
 * @property string $full_url
 * @property string $description
 * @property integer $status
 * @property \DateTime $date_create
 */
class LinkRecord extends ActiveRecord
{
    public static function tableName()
    {
        return 'link_data';
    }

    public function rules()
    {
        return [
            [['sort_order', 'status', 'user_id'], 'number'],
            [['title', 'full_url', 'description'], 'filter', 'filter' => 'trim'],
            [['user_id', 'title'], 'required'],
            ['title', 'string', 'max' => 100],
            [['full_url', 'description'], 'string', 'max' => 1000],
            ['full_url', 'url'],
            ['date_create', 'datetime', 'format' => 'php:Y-m-d H:i:s'],
        ];
    }

    public function fillModel(Link $link, TagsCollection $tagsCollection = null)
    {
        $link->setSortOrder($this->getAttribute('sort_order'));
        $link->setTitle($this->getAttribute('title'));
        $link->setUrl($this->getAttribute('full_url'));
        $link->setDescription($this->getAttribute('description'));
        $link->setStatus($this->getAttribute('status'));
        $link->setDateCreate(new \DateTime($this->getAttribute('date_create')));
        $link->setTagsCollection($tagsCollection);
    }
}