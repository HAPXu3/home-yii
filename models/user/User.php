<?php
/**
 * Created by PhpStorm.
 * User: HAPXu3
 * Date: 07.07.2018
 * Time: 18:48
 */

namespace app\models\user;

use yii\db\ActiveRecord,
    yii\web\IdentityInterface,
    yii\filters\RateLimitInterface;

/**
 * Class User
 * @package app\models\user
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $auth_key
 * @property string $access_token
 * @property \DateTime $date_create
 * @property integer $allowance
 * @property \DateTime $allowance_updated_at
 */
class User extends ActiveRecord implements IdentityInterface, RateLimitInterface
{
    public static function tableName()
    {
        return 'user';
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString();
                $this->date_create = (new \DateTime('now', new \DateTimeZone('Asia/Vladivostok')))->format('Y-m-d H:i:s');
            }
            if ($this->isAttributeChanged('password')) {
                $this->password = \Yii::$app->security->generatePasswordHash($this->password);
            }
            return true;
        }
        return false;
    }

    public function rules()
    {
        return [
            [['login', 'auth_key', 'password'], 'string', 'max' => 255],
        ];
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getRateLimit($request, $action)
    {
        return [100, 600];
    }

    public function loadAllowance($request, $action)
    {
        return [$this->allowance, $this->allowance_updated_at];
    }

    public function saveAllowance($request, $action, $allowance, $timestamp)
    {
        $this->allowance = $allowance;
        $this->allowance_updated_at = $timestamp;
        $this->save();
    }
}