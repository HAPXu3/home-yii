<?php

use yii\db\Migration;

/**
 * Handles the creation of table `link_tags_link_data`.
 * Has foreign keys to the tables:
 *
 * - `link_tags`
 * - `link_data`
 */
class m180813_130846_create_junction_table_for_link_tags_and_link_data_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('link_tags_link_data', [
            'link_tags_id' => $this->integer(),
            'link_data_id' => $this->integer(),
            'PRIMARY KEY(link_tags_id, link_data_id)',
        ]);

        // creates index for column `link_tags_id`
        $this->createIndex(
            'idx-link_tags_link_data-link_tags_id',
            'link_tags_link_data',
            'link_tags_id'
        );

        // add foreign key for table `link_tags`
        $this->addForeignKey(
            'fk-link_tags_link_data-link_tags_id',
            'link_tags_link_data',
            'link_tags_id',
            'link_tags',
            'id',
            'CASCADE'
        );

        // creates index for column `link_data_id`
        $this->createIndex(
            'idx-link_tags_link_data-link_data_id',
            'link_tags_link_data',
            'link_data_id'
        );

        // add foreign key for table `link_data`
        $this->addForeignKey(
            'fk-link_tags_link_data-link_data_id',
            'link_tags_link_data',
            'link_data_id',
            'link_data',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `link_tags`
        $this->dropForeignKey(
            'fk-link_tags_link_data-link_tags_id',
            'link_tags_link_data'
        );

        // drops index for column `link_tags_id`
        $this->dropIndex(
            'idx-link_tags_link_data-link_tags_id',
            'link_tags_link_data'
        );

        // drops foreign key for table `link_data`
        $this->dropForeignKey(
            'fk-link_tags_link_data-link_data_id',
            'link_tags_link_data'
        );

        // drops index for column `link_data_id`
        $this->dropIndex(
            'idx-link_tags_link_data-link_data_id',
            'link_tags_link_data'
        );

        $this->dropTable('link_tags_link_data');
    }
}
